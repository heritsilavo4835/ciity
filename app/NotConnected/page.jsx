import '@/app/NotConnected/page.css'

export default function NotConnected() {
    return <div className="h-100 row w-100 d-flex align-items-center justify-content-center __contain_not_connected">
        <div className="d-flex flex-column justify-content-between align-items-center rounded-3 col-11 col-sm-9 col-md-5">
            <button className="p-3 rounded-3 col-12 __auth_btn __auth_btn1">Creer un compte</button>
            <span className="mt-4 mb-4">ou</span>
            <button className="p-3 rounded-3 col-12 __auth_btn __auth_btn2">se connecter</button>
        </div>
    </div>
}