"use client"
import './page.css'
import Nav from '@/_components/Nav'
import Image from 'next/image'
import OneSignalement from '@/_components/OneSignalement'
import { useRouter } from 'next/navigation'
import { useContext,useState } from 'react'
import { signalementContext } from './layout'

export default function Signalement() {
    const router=useRouter()
    const [signalement,setSignalement] = useContext(signalementContext)
    const [dommaine,setDommaine]=useState("Propreté urbaine")

    const onSelect=function(e) {
      const curr=document.querySelector('.__selected__')
      curr.classList.remove('__selected__')

      const el=e.target
      el.classList.add('__selected__')
      setDommaine(el.innerHTML)
    }
    
    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 m-0 d-flex p-0 container-fluid flex-fill">
        <div className="d-flex flex-column p-0 container-fluid flex-fill m-0 mt-2">
          
          <div className='mt-5 d-flex justify-content-between align-items-center col-12 mt-2' style={{height:'60px',flexWrap:'wrap'}}>
            <span></span>
            <ul className='__dommaine_liste d-flex flex-wrap align-items-center'>
                <li onClick={onSelect} className='__selected__ border rounded p-1'>Propreté urbaine</li>
                <li onClick={onSelect} className='border rounded p-1'>Eclairage public</li>
                <li onClick={onSelect} className='border rounded p-1'>Voirie et chaussée</li>
                <li onClick={onSelect} className='border rounded p-1'>Stationnement</li>
                <li onClick={onSelect} className='border rounded p-1'>Equipement public</li>
                <li onClick={onSelect} className='border rounded p-1'>Espace vert</li>
            </ul>
          </div>

          <div style={{height:'80vh'}} className='mt-2 d-flex align-items-center justify-content-center col-12'>
            <ul className='rounded-2 h-75 col-11 __signal_liste'>
                {
                  signalement.map(function(element,i) {
                    if(element.domaine!=dommaine){
                      return null
                    }
                    return <OneSignalement img={element.imagePath} key={i} desc={element.description}></OneSignalement>
                  })
                }
            
            </ul>
          </div>
        </div>

        <button onClick={()=>{router.push('/signalement/signaler')}} title="ajouter un nouveau signalement" className='__add__signalement p-2 bg-primary'>+</button>
      </section>
    </div>
}