"use client"
import './page.css'
import Nav from '@/_components/Nav'
import Image from 'next/image'
import OneSignalement from '@/_components/OneSignalement'
import { useRouter } from 'next/navigation'
import Modal from '@/_components/Modal'
import { useState } from 'react'

export default function Signalement() {
    const router=useRouter()
    const [showModal,setShowModal]=useState(true)

    const closeAction=function() {
        router.push('/signalement')
    }

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Modal setShow={setShowModal} show={showModal} closeAction={closeAction} noFooter={true} noHeader={false}>
            <div style={{position:'relative',height:'20%'}} className='border col-12'>
                <Image fill style={{objectFit:'contain'}} alt='image-signalement' src="/probleme.jpg"></Image>
            </div>
            <div style={{position:'relative',height:'35%',overflow:"auto"}} className='border col-12'>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, consequatur provident. Eos atque dicta optio repudiandae tempora, exercitationem nostrum temporibus rem officiis minima iste natus, libero voluptatem, mollitia aspernatur nesciunt tenetur minus sequi explicabo placeat pariatur deserunt dignissimos! Voluptatibus minima aperiam adipisci nulla consequatur sapiente eum doloribus rem mollitia ea quaerat architecto corrupti harum nisi laudantium, obcaecati commodi neque dolor. Aspernatur voluptate blanditiis omnis architecto, molestias repudiandae sapiente et! Perferendis veniam, officiis laboriosam autem obcaecati eveniet, nesciunt vero corrupti ad accusantium necessitatibus modi sint asperiores? Voluptas atque ipsum facere qui, soluta eum. Necessitatibus ex tenetur esse quaerat minima temporibus eum illo voluptates distinctio. Fugiat necessitatibus maxime dolorem sed autem, iste commodi aut quaerat quisquam sapiente eum, non odit? Illum, obcaecati omnis. Architecto expedita qui laudantium quos, accusamus excepturi autem cum fugiat placeat ducimus sunt repellendus deleniti quia accusantium numquam quo totam perspiciatis, unde mollitia? Ullam optio quos voluptate maxime voluptatem iure, reprehenderit totam tempora quaerat aliquam nulla vel minus? Tempora, similique. Labore aliquid dolorem dolor hic amet architecto. Sed qui illo assumenda eveniet eius aspernatur quidem a? Ducimus asperiores quibusdam ex expedita adipisci quis in nobis ipsum accusamus blanditiis, totam inventore harum veniam voluptate dolorem! Accusantium deleniti dignissimos corrupti animi necessitatibus praesentium voluptas reiciendis voluptate quidem quibusdam excepturi sapiente molestias asperiores qui voluptatum aliquid distinctio et, culpa quod cumque, facilis expedita fugiat! Illo facere ducimus delectus sequi pariatur, maxime ipsa doloribus deleniti aut corporis quaerat soluta id beatae nulla in? Velit beatae praesentium saepe, nam possimus nobis! Deserunt quidem totam incidunt quia qui aspernatur eaque fuga repudiandae earum inventore repellendus voluptatibus expedita ab consequuntur provident architecto nobis laboriosam officiis dolorum porro, placeat aut! Laudantium in totam expedita dolor vel distinctio nisi laborum asperiores aspernatur perspiciatis modi quae, quaerat, qui nemo officiis ipsam dignissimos, reiciendis ea omnis! Fugit, culpa. Ratione, quis.
            </div>

            <div style={{position:'relative',height:'5%',overflow:"hidden"}} className='border d-flex align-items-center justify-content-between col-12'>
                <button className="p-1 border rounded __btn_vrai_faux __btn_vrai">Vrai</button>
                <button className="p-1 border rounded __btn_vrai_faux __btn_faux">Fake</button>
                <span> non resolu </span>
            </div>
        </Modal>
        <Nav></Nav>
        <section className="__sec_1 m-0 d-flex p-0 container-fluid flex-fill">
        <div className="d-flex flex-column p-0 container-fluid flex-fill m-0 mt-2 ">
          
          <div className='mt-5 d-flex justify-content-between align-items-center col-12 mt-2' style={{height:'60px',flexWrap:'wrap'}}>
            <span></span>
            <ul className='__dommaine_liste d-flex flex-wrap align-items-center'>
                <li className='border rounded p-1'>Propreté urbaine</li>
                <li className='border rounded p-1'>Eclairage public</li>
                <li className='border rounded p-1'>Voirie et chaussée</li>
                <li className='border rounded p-1'>Stentionnement</li>
                <li className='border rounded p-1'>Equipement public</li>
                <li className='border rounded p-1'>Espace vert</li>
            </ul>
          </div>

          <div style={{height:'80vh'}} className='mt-2 d-flex align-items-center justify-content-center col-12'>
            <ul className='rounded-2 h-75 col-11 __signal_liste'>
                <OneSignalement></OneSignalement>
                <OneSignalement></OneSignalement>
                <OneSignalement></OneSignalement>
            </ul>
          </div>
        </div>

        <button onClick={()=>{router.push('/signalement/signaler')}} title="ajouter un nouveau signalement" className='__add__signalement p-2 bg-primary'>+</button>
      </section>
    </div>
}