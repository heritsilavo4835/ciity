"use client"
import { createContext,useState } from 'react'

const S=[
    {
      "id": 16,
      "description": "Dépôt sauvage de déchets près du parc Kennedy.",
      "imagePath": "/probleme.jpg",
      "domaine": "Propreté urbaine"
    },
    {
      "id": 17,
      "description": "Tag sur le mur de la bibliothèque municipale.",
      "imagePath": "/probleme.jpg",
      "domaine": "Propreté urbaine"
    },
    {
      "id": 18,
      "description": "Lampadaire clignotant à l'angle de la rue St-Paul.",
      "imagePath": "/probleme.jpg",
      "domaine": "Eclairage public"
    },
    {
      "id": 19,
      "description": "Ampoule grillée près du marché central.",
      "imagePath": "/probleme.jpg",
      "domaine": "Eclairage public"
    },
    {
      "id": 20,
      "description": "Route fissurée à l'entrée du quartier résidentiel.",
      "imagePath": "/probleme.jpg",
      "domaine": "Voirie et chaussée"
    },
    {
      "id": 21,
      "description": "Marquage au sol effacé devant l'hôtel de ville.",
      "imagePath": "/probleme.jpg",
      "domaine": "Voirie et chaussée"
    },
    {
      "id": 22,
      "description": "Véhicule mal stationné bloquant la circulation.",
      "imagePath": "/probleme.jpg",
      "domaine": "Stationnement"
    },
    {
      "id": 23,
      "description": "Panneau de stationnement endommagé sur la rue des Roses.",
      "imagePath": "/probleme.jpg",
      "domaine": "Stationnement"
    },
    {
      "id": 24,
      "description": "Banc de parc vandalisé près du quartier des artistes.",
      "imagePath":"/probleme.jpg",
      "domaine": "Equipement public"
    },
    {
      "id": 25,
      "description": "Aire de jeu hors service au parc des enfants.",
      "imagePath":"/probleme.jpg",
      "domaine": "Equipement public"
    },
    {
      "id": 26,
      "description": "Espaces verts non entretenus le long de l'avenue principale.",
      "imagePath": "/probleme.jpg",
      "domaine": "Espace vert"
    },
    {
      "id": 27,
      "description": "Fleurs fanées dans le jardin public.",
      "imagePath": "/probleme.jpg",
      "domaine": "Espace vert"
    }
  ]
  
  
export const signalementContext=createContext();

export default function Lay({children}) {
    const [signalement,setSignalement]=useState(S)

    return <signalementContext.Provider value={[signalement,setSignalement]}> {children} </signalementContext.Provider>
}