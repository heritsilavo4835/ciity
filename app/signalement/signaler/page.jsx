"use client"
import './page.css'
import {useState,useContext,useRef} from 'react'
import { useRouter } from 'next/navigation'
import { signalementContext } from '../layout'

export default function Signaler() {
    const [file,setFile]=useState(null)
    const [signalement,setSignalement]=useContext(signalementContext)
    const router=useRouter();
    const textareaRef=useRef()
    const selectRef=useRef()

    const onSubmit=function(e) {
        e.preventDefault()
        const obj={
            "id": 27,
            "description": textareaRef.current.value,
            "imagePath": "/probleme.jpg",
            "domaine": selectRef.current.value
        }
        const tmp=[...signalement]
        tmp.push(obj)
        setSignalement(tmp)
        router.push('/signalement')
    }

    return <div className="__contain_form_signaler container-fluid align-items-center justify-content-center d-flex m-0 p-0 h-100">
        <div style={{height:'90%'}} className='__form_signaler rounded-2 col-11 col-sm-7 col-md-6 border m-0'> 
            <h3 className='text-center mt-5'>Signaler un probleme</h3>
            <form onSubmit={onSubmit} className="mt-5 col-10 m-auto" style={{height:'65%'}}>
                <div className="desc col-10 m-auto">
                    <p className='col-12 m-auto mt-3'> Dans quelle dommaine vous volez signaler?:</p>
                    <select ref={selectRef} className='form-control' name="dommaine" id="dommaine">
                        <option value="Propreté urbaine">Propreté urbaine</option>
                        <option value="Eclairage public">Eclairage public</option>
                        <option value="Voirie et chaussée">Voirie et chaussée</option>
                        <option value="Stationnement">Stationnement</option>
                        <option value="Equipement public">Equipement public</option>
                        <option value="Espace vert">Espace vert</option>
                    </select>    
                </div>
                
                <div className="desc col-10 m-auto">
                    <p className='col-10 m-auto mt-3'> Description du probleme:</p>
                    <textarea ref={textareaRef} style={{maxHeight:'300px'}} className="col-12"></textarea>
                </div>

                <div className="desc d-flex justify-content-between col-10 m-auto">
                    <button onClick={()=>{router.back()}} type="reset" className='p-2 rounded _btn_signaler _btn_signaler1'>Annuler</button>
                    <button type='submit' className='p-2 rounded _btn_signaler _btn_signaler2'>Confirmer</button>
                </div>
            </form>
        </div>
    </div>
}