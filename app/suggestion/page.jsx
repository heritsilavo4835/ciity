"use client"
import './page.css'
import Nav from '@/_components/Nav'
import Image from 'next/image'
import OneSuggestion from '@/_components/OneSuggestion'
import { useRouter } from 'next/navigation'

export default function Signalement() {
    const router=useRouter()

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 m-0 d-flex p-0 container-fluid flex-fill">
        <div className="d-flex flex-column justify-content-center p-0 container-fluid flex-fill m-0 mt-2">

          <div style={{height:'80vh'}} className=' mt-2 d-flex flex-column align-items-center justify-content-center col-12'>
            <h3>Vennez un peu voir les suggestions des autres pour ameliorer votre cadre de vie </h3>

            <ul style={{height:"90%"}} className='rounded-2 col-11 __signal_liste'>
                <OneSuggestion></OneSuggestion>
                <OneSuggestion></OneSuggestion>
                <OneSuggestion></OneSuggestion>
            </ul>
          </div>
        </div>

        <button onClick={()=>{router.push('/suggestion/suggerer')}} title="ajouter un nouveau signalement" className='__add__signalement p-2 bg-primary'>+</button>
      </section>
    </div>
}