"use client"
import './page.css'
import {useState} from 'react'
import { useRouter } from 'next/navigation'

export default function Signaler() {
    const [file,setFile]=useState(null)
    const router=useRouter();

    return <div className="__contain_form_signaler container-fluid align-items-center justify-content-center d-flex m-0 p-0 h-100">
        <div style={{height:'90%'}} className='__form_signaler rounded-2 col-11 col-sm-7 col-md-6 border m-0'> 
            <h3 className='text-center mt-5'> Boite a idee </h3>
            <form className="mt-5 col-10 m-auto" style={{height:'65%'}}>
                <div className="desc col-10 m-auto">
                    <p className='col-10 mt-3'> Que suggerez-vous?</p>
                    <textarea style={{maxHeight:'300px',minHeight:'300px'}} className="col-12"></textarea>
                </div>

                <div className="desc d-flex justify-content-between col-10 m-auto">
                    <button onClick={()=>{router.back()}} type="reset" className='p-2 rounded _btn_signaler _btn_signaler1'>Annuler</button>
                    <button type='submit' className='p-2 rounded _btn_signaler _btn_signaler2'>Confirmer</button>
                </div>
            </form>
        </div>
    </div>
}