import '@/app/signup/page.css'

export default function Signup() {
    return  <div className="h-100 row w-100 d-flex align-items-center justify-content-center __contain_not_connected">

        <form className="d-flex flex-column justify-content-between align-items-center rounded-3 col-11 col-sm-9 col-md-5 col-lg-4">
            <h3 className="mb-5">Se Connecter</h3>

            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="text" name="Nom" id="Nom" />
                <label className='__label' htmlFor="Nom">Nom</label>
            </div>
        
            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="text" name="Nom" id="Nom" />
                <label className='__label' htmlFor="Nom">Mot De passe</label>
            </div>

            <div className="m-3 d-flex align-items-center justify-content-between col-12 __button_box">
                <button className="p-2 pb-1 pt-1 border-1 rounded __sign_up_btn __sign_up_btn1">Annuler</button>
                <button className="p-2 pb-1 pt-1 border-1 rounded __sign_up_btn __sign_up_btn2">Se connecter</button>
            </div>
        </form>
    </div>
}