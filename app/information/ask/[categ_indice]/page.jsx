"use client"
import './page.css'
import Nav from '@/_components/Nav'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {useContext} from 'react'
import { dataContext } from '../../layout'
import { useEffect,useState } from 'react' 
import { useRouter } from 'next/navigation'
import SearchInfos from '@/_components/SearchInfos'

export default function Informer({params}) {
    const fileData=useContext(dataContext);
    const [info,setInfo]=useState();
    const router = useRouter()

    useEffect(function() {
        if (fileData.length != 0) {
            const data=fileData[params.categ_indice]
            setInfo(data)
        }
    },[fileData])

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 d-flex flex-column p-0 container-fluid flex-fill">
            <div className="d-flex align-items-center justify-content-center text-white h-25  __searchBox col-12 ">
                <SearchInfos></SearchInfos>
            </div>

            <div style={{height:'65%'}}  className="d-flex flex-column align-items-center justify-content-start col-12 ">
                <div className='d-flex flex-column align-items-center border _type_container col-10 flex-fill'> 
                    <h2 className='text-center'> {info?.categorie} </h2>
                    {
                        info?.data?.map(function(element,i) {
                            return <div onClick={()=>{router.push("/information/ask/" + params.categ_indice+"/"+i )}} key={i} className='__info__ bg-primary-subtle border p-2 mt-2 rounded-3 col-10'> { element?.Titre } </div>
                        })
                    }
                
                </div>
            </div>
        </section>
    </div>
}