"use client"
import './page.css'
import Nav from '@/_components/Nav'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {useContext} from 'react'
import { dataContext } from '../../../layout'
import { useEffect,useState } from 'react' 
import SearchInfos from '@/_components/SearchInfos'

export default function Informer({params}) {
    const fileData=useContext(dataContext);
    const [tuto,setTuto]=useState();
    
    useEffect(function() {
        if (fileData.length != 0) {
            const data=fileData[params.categ_indice].data[params.info_indice]
            setTuto(data)
            console.log(data);
        }
    },[fileData])

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 d-flex flex-column p-0 container-fluid flex-fill">
            <div className="d-flex align-items-center justify-content-center text-white h-25  __searchBox col-12 ">
                <SearchInfos></SearchInfos>  
            </div>

            <div style={{height:'65%'}}  className="d-flex flex-column align-items-center justify-content-start col-12 ">
                
                
                <div className='d-flex flex-column align-items-center rounded-3 border _type_container col-11 flex-fill'> 
                    <h1 className='text-center'> { tuto?.Titre } </h1>
                    <span className='align-self-start m-2'> { tuto?.description } </span>

                    {
                        tuto?.Steps?.map(function(element,i) {                            
                            if(element.etape_type=='Sous-titre'){
                                return <h2 key={i+'h2'} className='__sous_titre m-2 align-self-start'> {element.etape_desc} </h2>
                            }else if (element.etape_type=='Sous-sous-titre'){
                                return <h5 key={i+'h5'} className='__sous_sous_titre col-12 m-2 align-self-start'> {element.etape_desc} </h5>
                            }else{
                                return <>
                                    <h5 key={i+'titre'} className='m-2 align-self-start'> {element.etape_titre} </h5>
                                    <p key={i+'p'} className='m-2 align-self-start'>{element.etape_desc}</p>
                                </>
                            }
                        })
                    }
                </div>
            </div>
        </section>
    </div>
}