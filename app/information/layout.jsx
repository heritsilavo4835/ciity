"use client"
import { useEffect,useState } from 'react'
import axios from 'axios'
import { createContext } from 'react'

export const dataContext=createContext()

export default function Layout_infos({children}) {
    const [fileData,setFileData]=useState([])
    
    useEffect(function() {
        let tmp=[]
        getData("/data/Agriculture_et_elevage.json").then(function({data}) {
            tmp.push({ categorie:" Agriculture et elevage ",data:data })            
            return getData("/data/Citoyenete.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Cytoyeneté",data:data })
            return getData("/data/Entreprises_et_Industrie.json")
        }).then(function({data}) {
            tmp.push({ categorie:" Entreprise et industrie ",data:data })
            return getData("/data/Etat_Civile.json")
        }).then(function({data}) {
            tmp.push({ categorie:" État civile ",data:data })
            return getData("/data/Fiscalite.json")
        }).then(function({data}) {
            tmp.push({ categorie:" Fiscalité",data:data })
            return getData("/data/Habitat_et_Foncier.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Habitat et Foncier",data:data })
            return getData("/data/Plainte.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Plainte",data:data })
            return getData("/data/Tarvail_Administratif.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Travail Administratifs",data:data })
            return getData("/data/Urgence.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Urgence",data:data })
            return getData("/data/Voyage.json")
        }).then(function({data}) {
            tmp.push({ categorie:"Voyage",data:data })
        }).finally(function() {
            setFileData(tmp)
        })
    },[])

    return <dataContext.Provider value={fileData}>
            {children}
        </dataContext.Provider>
}


function getData(url) {
   return axios.get(url)
}