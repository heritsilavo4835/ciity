"use client"
import './page.css'
import Nav from '@/_components/Nav'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {useContext} from 'react'
import { useRouter } from 'next/navigation'
import { dataContext } from './layout'
import SearchInfos from '@/_components/SearchInfos'

export default function Informer() {
    const fileData=useContext(dataContext);
    const router=useRouter();

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 d-flex flex-column p-0 container-fluid flex-fill">
            <div className="d-flex align-items-center justify-content-center text-white h-25  __searchBox col-12 ">
                <SearchInfos></SearchInfos>
            </div>

            <div style={{height:'65%'}}  className="d-flex flex-column align-items-center justify-content-start col-12 ">
                <h3>Quelles types d'informations vous rechercher?</h3>
                <div className='_type_container col-10 flex-fill d-flex flex-wrap'> 
                    {
                        fileData.map(function(element,i) {
                            return <div onClick={()=>{router.push('/information/ask/'+i)}} key={i} className='__type bg-primary-subtle m-2 rounded-2 d-flex flex-wrap align-items-center justify-content-center'> {element.categorie} </div>
                        })
                    }
                </div>
            </div>
        </section>
    </div>
}