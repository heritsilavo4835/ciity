"use client"
import './page.css'
import Nav from '@/_components/Nav'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {useContext} from 'react'
import { dataContext } from '../../layout'
import { useEffect,useState } from 'react' 
import { useRouter } from 'next/navigation'
import SearchInfos from '@/_components/SearchInfos'

export default function Informer({params}) {
    const fileData=useContext(dataContext);
    const [searchRes,setSearchRes]=useState();
    const router=useRouter()

    useEffect(function() {
        let tmp=[]
        if (fileData.length != 0) {
            fileData.forEach((element,i) => {
                element.data.forEach((tutoriel,j) => {
                    if(tutoriel.key.toLowerCase().includes(params.search_key.toLowerCase())){
                        tutoriel={...tutoriel,categ_i:i,info_i:j}
                        console.log(tutoriel);
                        tmp.push(tutoriel)
                    }
                });
            });
        }
        setSearchRes(tmp)
    },[fileData])

    return <div className='container-fluid h-100 p-0 d-flex flex-column'>
        <Nav></Nav>
        <section className="__sec_1 d-flex flex-column p-0 container-fluid flex-fill">
            <div className="d-flex align-items-center justify-content-center text-white h-25  __searchBox col-12 ">
                <SearchInfos search_key={params.search_key}></SearchInfos>
            </div>

            <div style={{height:'65%'}}  className="d-flex flex-column align-items-center justify-content-start col-12 ">
                <div className='d-flex flex-column align-items-center rounded-3 border _type_container col-10 flex-fill'> 
                <h2 className='text-center'> Resultat de recherche </h2>
                    {
                        searchRes?.map(function(element,i) {
                            return <div onClick={()=>{router.push("/information/ask/" + element.categ_i+"/"+element.info_i )}} key={i} className='__info__ bg-primary-subtle border p-2 mt-2 rounded-3 col-10'> { element?.Titre } </div>
                        })
                    }
                </div>
            </div>
        </section>
    </div>
}