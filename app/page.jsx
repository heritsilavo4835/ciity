"use client"
import Image from 'next/image'
import './page.css'
import { useEffect,useState } from 'react'
import axios from 'axios'
import dynamic from 'next/dynamic'

const Nav = dynamic(
  ()=>import('@/_components/Nav'),
  {ssr:false}
)

const API_KEY = '5c5f260606dcb121f91a5e6f473425f6';

export default function Home() {
  const [temperature,setTemperature]=useState(0)

  useEffect(()=>{
    (async ()=>{
      const ville="Fianarantsoa"
      const API_URL = `https://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=${API_KEY}`;
      const response = await axios.get(API_URL);
      const data = response.data;
      setTemperature(data.main.temp)
    })();  
  },[])

  return (
    <div id="root" className="d-flex flex-column">
      <Nav> </Nav>
      <section className="__sec_1 d-flex p-0 container-fluid" style={{height:'450px'}}>
        <div className="d-flex p-0 container-fluid flex-fill m-0 __contain_acceuil">
          <div className='mt-5 d-flex flex-column align-items-start col-9 col-sm-'>
            <h2 className='mt-5 col-10'>Rendre Votre vie de citoyen plus facile graces aux nouvelles technologies</h2>
            <button className='__btn_savoir_plus p-2 rounded mt-3'> <a id="__savoir_plus_link" href="#serviceProposees"> En savoir plus </a> </button>
          </div>
          <div className='col d-none   d-md-flex align-items-start justify-content-end'> 
            <div style={{fontSize:'1.5em'}} className="p-2 text-white rounded-1 m-2 __weather_container">
              {Math.floor(temperature-273.15)}°C
            </div>
          </div>
        </div>
      </section>

      <h1 id='serviceProposees' className='mt-5 text-center'>Quelles services nous proposons? </h1>
      <section className="__sec_2 d-flex flex-column p-0 container-fluid">
          <article className="mt-2 pt-2 d-flex container-fluid" style={{height:"300px"}}>
            <div className=" col-6 ">
              <h4 className="m-3">Information</h4>
              <ul className="m-3">
                <li>Des informations sur comment preparer les lettres d'administrations comme: passeport,CIN,...</li>
              </ul>
            </div>
            <div className="__img_container col-6 ">
              <Image fill style={{objectFit:'contain'}} alt='image-information' src="/images.jpeg"></Image>
            </div>
          </article>

          <article className="mt-2 pt-2 d-flex container-fluid" style={{height:"300px"}}>
            <div className="__img_container col-6 ">
            <Image fill style={{objectFit:'contain'}} alt='image-idees' src="/idee.jpg"></Image>
            </div>
            <div className=" col-6 ">
              <h4 className="m-3">Faire des suggestion pour le developpement</h4>
              <p className="m-3">Permettre aux usagers de donner des suggestion pour ameliorer leurs cadre de vie dans la ville et les autres peuvent voter pour aprouver ou non la suggestion.</p>
            </div>
          </article>

          <article className="mt-2 pt-2 d-flex container-fluid" style={{height:"300px"}}>
            <div className=" col-6 ">
              <h4 className="m-3">Signaler un probleme</h4>
              <p className="m-3">Donner la possibilite de signaler un probleme et les gens pourrait aussi confirmer si le proble existe deja et apres les membre du gouvernement pourrait marquer le probleme commme encours de traitement pour faire savoir au public qu'ils cherchent une solution au probleme signalé</p>
            </div>
            <div className="__img_container col-6 ">
            <Image fill style={{objectFit:'contain'}} alt='image-information' src="/probleme.jpg"></Image>
            </div>
          </article>
      </section>
    </div>
  )
}
