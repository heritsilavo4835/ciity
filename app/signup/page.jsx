"use client"
import '@/app/signup/page.css'
import axios from 'axios'
import { useEffect } from 'react'
axios.defaults.baseURL="http://localhost:xxxx"


export default function Signup() {
    const handleSubmit=function(event) {
        event.preventDefault();
        //requete vers la base de donnees

    }

    return  <div className="h-100 row w-100 d-flex align-items-center justify-content-center __contain_not_connected">
        <form onSubmit={handleSubmit} className="d-flex flex-column justify-content-between align-items-center rounded-3 col-11 col-sm-9 col-md-5 col-lg-4">
            
            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="text" name="Nom" id="Nom" />
                <label className='__label' htmlFor="Nom">Nom d'utilisateur</label>
            </div>
        
            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="email" name="email" id="email" />
                <label className='__label' htmlFor="Nom">email</label>
            </div>

            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="text" name="Ville" id="Ville" />
                <label className='__label' htmlFor="Nom">Ville</label>
            </div>

            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="text" name="Adresse" id="Adresse" />
                <label className='__label' htmlFor="Nom">Adresse</label>
            </div>

            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="password" name="MDP" id="Nom" />
                <label className='__label' htmlFor="Nom">Mot de passe</label>
            </div>

            <div className="border mb-3 rounded-3  __input_box col-12">
                <input required className="__input_text" type="password" name="confirm_Nom" id="confirm_Nom" />
                <label className='__label' htmlFor="Nom">Confirmer le mot de passe</label>
            </div>

            <div className="m-3 d-flex align-items-center justify-content-between col-12 __button_box">
                <button className="p-2 pb-1 pt-1 border-1 rounded __sign_up_btn __sign_up_btn1">Annuler</button>
                <button className="p-2 pb-1 pt-1 border-1 rounded __sign_up_btn __sign_up_btn2">Creer mon compte</button>
            </div>
        </form>
    </div>
}