"use client"
import '@/_components/css/SearchInfos.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/navigation'
import { useRef } from 'react'

export default function SearchInfos({search_key}) {
    const router =useRouter()
    const inputRef=useRef(null)

    const handleSubmit=function(e) {
        e.preventDefault();
        if(inputRef.current.value.trim()!=""){
            router.push('/information/search/'+inputRef.current.value); 
        }else{
            router.push('/information');
        }
    }

    return <form onSubmit={handleSubmit} className="rounded-5 d-flex justify-content-between align-items-center col-9 h-50 __input_search_box border">
        <input ref={inputRef} placeholder='Rechercher' defaultValue={search_key ? search_key : ''} className='__input_search' type="search" name="search" id="search" />
        <button className='__btn_search m-1  d-flex justify-content-center align-items-center'> <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon> </button>
    </form>
}