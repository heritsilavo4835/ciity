import '@/_components/css/Modal.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClose } from '@fortawesome/free-solid-svg-icons'

export default function Modal({children,titre,show,setShow,noHeader,noFooter,setConfirm,closeAction}){
    return <>
        {
            show && (
                <div className="d-flex align-items-start justify-content-center" id="__modal__container">
                    <div className="mt-3 col-4 rounded" id="__modal__">
                        {(!noHeader) && (
                            <div className="p-1 col-12 d-flex align-items-center " id="__modal_header">
                                <h5>{titre?titre:""}</h5>
                                <span onClick={(closeAction ? (closeAction) : ()=>{setShow(false)})} id='__close_btn_'> <FontAwesomeIcon icon={faClose}></FontAwesomeIcon> </span>
                            </div>
                        )}
                        <div className="p-1 col-12" id="__modal_content">
                            {children}
                        </div>
                        {
                            (!noFooter) && (
                                <div className="d-flex align-item-center justify-content-end col-12 mt-2 p-1" id="__modal_footer">
                                    <button id='__btns__' onClick={(closeAction ? (closeAction) : ()=>{setShow(false)})} className='p-1 rounded m-2 mt-0 mb-0'>Annuler</button>
                                    <button id='__btns__' onClick={()=>{setConfirm(true);setShow(false)}} className='p-1 rounded m-2 mt-0 mb-0'>Confirmer</button>
                                </div>
                            )
                        }
                    </div>
                </div>
            )
        }
    </>
}