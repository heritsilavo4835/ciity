"use client"
import '@/_components/css/OneSuggestion.css'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp,faThumbsDown, faTrash, faEdit } from '@fortawesome/free-solid-svg-icons'

export default function OneSuggestion() {
    const router =useRouter()
    return <li className='border mt-2 d-flex flex-column rounded'>
    <div className='border d-flex justify-content-between align-items-center col-12' style={{position:'relative',height:'45px',cursor:'text'}}>
        <div  className='h-100 d-flex align-items-center'>
            <div style={{cursor:'pointer'}} className='bg-dark __profil__img'></div>
            <span style={{cursor:'pointer'}} className=''>@username</span>
        </div>
        <div  style={{fontSize:"1.3em",cursor:'pointer',position:'relative'}} className='m-3 mt-0 mb-0 d-none'>
            <FontAwesomeIcon className='m-3 mt-0 mb-0' icon={faEdit}></FontAwesomeIcon>
            <FontAwesomeIcon className='m-3 mt-0 mb-0' icon={faTrash}></FontAwesomeIcon>
        </div>
    </div>
    <div style={{position:'relative',height:'72%',overflow:"auto"}} className='border col-12'>
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laboriosam, a. Omnis quos beatae, aut ea atque eveniet voluptates similique distinctio nulla officia quis repudiandae blanditiis numquam culpa ab iure quam debitis corrupti excepturi aspernatur aperiam? Voluptas consequuntur excepturi laboriosam facere commodi quia inventore dolores dolorem similique. Accusantium, quia ipsa placeat, illum, incidunt assumenda architecto laudantium delectus dolores aspernatur ratione totam similique! Repellat labore incidunt facere doloremque error, pariatur ipsam eligendi unde cupiditate consectetur quibusdam, earum molestiae quas sunt autem iure, placeat id aperiam natus asperiores eaque. Excepturi sint cum quisquam a harum officiis autem. Sunt ab quidem consequuntur eaque cumque placeat qui obcaecati magnam. Voluptas ullam eius sunt est amet harum neque qui cum non, quae iure praesentium. Rem asperiores illo iste, sapiente suscipit ad voluptate, amet aut porro molestias delectus labore. Maxime non culpa inventore beatae delectus, temporibus quam deserunt at possimus odio maiores soluta, libero exercitationem recusandae, vitae assumenda repudiandae nemo consequatur laboriosam aliquam! Corporis debitis, quod earum repellat alias ipsa vel animi mollitia sequi nam dolor quia eaque adipisci veritatis doloribus fugit minus facilis enim accusantium dolore perferendis veniam voluptatibus est. Harum ipsum quisquam, beatae dolor omnis atque minima sunt deserunt consequuntur quasi impedit illum, pariatur tempore provident sequi numquam eum ea enim reiciendis praesentium eligendi vitae amet obcaecati. Soluta repellendus possimus nihil ipsum explicabo architecto, voluptatum nostrum eveniet, delectus consectetur distinctio ea qui? Itaque ab sit nesciunt voluptate deleniti nulla, maxime mollitia error ducimus nam iusto saepe recusandae fuga cumque quae aliquid culpa molestiae soluta, adipisci voluptatibus dolore possimus facilis consequatur hic? Accusantium temporibus incidunt adipisci illum, dolore, quasi repellat placeat dolores quos, velit recusandae laboriosam hic mollitia quis ut omnis dicta voluptas. Nulla ullam esse, sit voluptas omnis, deserunt, dicta sequi at cumque illo quisquam? Fuga animi deleniti deserunt expedita error veritatis quaerat, officia totam.
    </div>

    <div style={{position:'relative',height:'13%',overflow:"hidden"}} className=' d-flex align-items-center justify-content-start col-12'>
        <button className="p-1 m-2 mb-0 mt-0 border rounded __btn_vrai_faux __btn_vrai"> <FontAwesomeIcon icon={faThumbsUp}></FontAwesomeIcon> </button>
        <button className="p-1 m-2 mb-0 mt-0 border rounded __btn_vrai_faux __btn_faux"><FontAwesomeIcon icon={faThumbsDown}></FontAwesomeIcon></button>
    </div>
</li>
}