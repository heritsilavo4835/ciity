export default function FilePreview({ file }) {
    const { name } = file;
    return (
      <div data-testid="__file_preview">
        <p className="badge text-primary">{name}</p>
      </div>
    );
  }
  