import '@/_components/css/Nav.css'
import Link from 'next/link'
import Image from 'next/image'

export default function Nav() {
    return <nav className="container-fluid d-flex align-items-center justify-content-between">
        <div style={{position:'relative'}} className="logo h-100">
            <Image fill style={{objectFit:'contain'}} alt='image-idees' src="/logo.png"></Image>
        </div>
        <ul>
            <li> <Link href='/'>Accueil</Link> </li>
            <li> <Link href='/information'>Informations</Link> </li>
            <li> <Link href='/signalement'>Signalement</Link> </li>
            <li> <Link href='/suggestion'>Suggestion</Link> </li>
            <li> Se connecter </li>
        </ul>
    </nav>
}