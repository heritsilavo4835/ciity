"use client"
import '@/_components/css/OneSignalement.css'
import Image from 'next/image'
import { useRouter } from 'next/navigation'

export default function OneSignalement({desc,img}) {
    const router =useRouter()

    return <li className='border mt-2 d-flex flex-column rounded'>
    <div style={{position:'relative',height:'40%'}} className='border col-12'>
        <Image fill style={{objectFit:'contain'}} alt='image-signalement' src={img}></Image>
    </div>
    <div style={{position:'relative',height:'45%',overflow:"hidden"}} className='border col-12'>
        {desc}
    </div>

    <div style={{position:'relative',height:'15%',overflow:"hidden"}} className='border d-flex align-items-center justify-content-between col-12'>
        <button className="p-1 border rounded __btn_vrai_faux __btn_vrai">Vrai</button>
        <button className="p-1 border rounded __btn_vrai_faux __btn_faux">Fake</button>
        <span> non resolu </span>
    </div>
</li>
}