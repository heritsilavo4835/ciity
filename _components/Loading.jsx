"use client"
import React from "react";
import '@/components/css/Loading.css'

const Loader = ({dark}) => {
  return <>
    <div data-testid='__loader' className={"circle-spin-7 "+(dark?' bg-dark ':'')}></div>
  </>
};

export default Loader;